﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace QuizdosHarvey.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string cedula { get; set; }

        public string nombres { get; set; }

        public string apellidos { get; set; }

        public TimeSpan fecha_nacimiento { get; set; }
        
    }
}
